FROM python:3.7-alpine

WORKDIR /konnektbox-security
ENV PYTHONUNBUFFERED=1

RUN mkdir ./alerts
RUN apk add --no-cache --update gcc musl-dev linux-headers
COPY requirements.txt ./
RUN python3 -m pip install -r requirements.txt
COPY . .

ENTRYPOINT [ "python3", "app.py", "-json", "/konnektbox-security/logs/alerts/alerts.json", "-callback", "modules.callback.mqtt_pub_callback"]
