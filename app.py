# Standard imports
import sys
import signal
from threading import Event

# Modules
from modules.config import Config
from modules.mqtt_handler import MqttClient
from modules.wazuh_log_analyzer import WazuhLogAnalyzer

_BUFFER_SIZE = 8132
_DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S.%f%z"

# Config parameters and globals
config = Config()


def main(args):
    """ Main app """
    print('Launching ELASTIC alert handler...')

    def clean_up(sig, frame):
        config.logger.info("Clean up done")
        mqtt_client.stop()
        sys.exit()

    # Launch MQTT client
    mqtt_client = MqttClient()
    mqtt_client.start()

    # Launch Wazuh Log analyzer
    # args.callback = mqtt_pub_callback
    wazuh_log_analyzer = WazuhLogAnalyzer(args, mqtt_client)
    wazuh_log_analyzer.start()

    # Signal Handling
    signal.signal(signal.SIGINT, clean_up)
    signal.signal(signal.SIGTERM, clean_up)

    while True:
        Event().wait()


if __name__ == "__main__":
    import argparse
    print(__import__(__name__.split('.')[0]))

    parser = argparse.ArgumentParser()
    parser.add_argument('-json', required=False,
                        help="a json file")
    parser.add_argument('-filters', required=False,
                        help="")
    parser.add_argument('-datetime_field', default=None,
                        required=False,
                        help="")
    parser.add_argument('-datetime_format', default=_DATETIME_FORMAT,
                        required=False,
                        help="")
    parser.add_argument('-start_datetime',  # type=datetime.datetime.fromisoformat, only supported in py37
                        required=False,
                        help="2020-04-06T10:22:00")
    parser.add_argument('-end_datetime',  # type=datetime.datetime.fromisoformat, only supported in py37
                        required=False,
                        help="2020-04-06T13:22:00")
    parser.add_argument('-limit', type=int,
                        required=False,
                        help="how many results to return")
    parser.add_argument('-realtime', action='store_true',
                        required=False,
                        help="continous and realtime reading, takes only the latest entries")
    parser.add_argument('-echo', action='store_true',
                        required=False,
                        help="print processed lines in raw format and exit")
    parser.add_argument('-callback',
                        required=False,
                        help="a custom python function to be called on each occurrence")
    parser.add_argument('-pretty', action='store_true',
                        required=False, default=True,
                        help="print indented json")
    parser.add_argument('-bufsize', type=int,
                        required=False, default=_BUFFER_SIZE,
                        help="set a proper buffer size")
    parser.add_argument('-timeout', type=float,
                        default=0.112,
                        required=False,
                        help="how many seconds or milliseconds the reader wait before reading in realtime or stdin loop")
    args = parser.parse_args()

    main(args)
