import datetime
import io
import gzip
import json
import operator
import re
import pytz

import sys
import time

from threading import Thread

from functools import reduce
from importlib import import_module

from modules.mqtt_handler import MqttClient


_DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S.%f%z"
_DATETIME_FORMAT_INPUT = _DATETIME_FORMAT[:-5]

_REALTIME_DELIM = '\n'


def _get_fblocks(fstr):
    """
    from 'a and b or c OR d and e or gol'
    returns
    [['a', 'b'], ['c'], ['d', 'e'], ['gol']]
    """
    orf = [i.strip() for i in re.split(' (or|OR) ', fstr) if i not in ('or', 'OR')]
    for ind in range(len(orf)):
        andf = [i.strip() for i in re.split(' (and|AND) ', orf[ind]) if i not in ('and', 'AND')]
        if len(andf) > 1:
            orf[ind] = andf
        if isinstance(orf[ind], str):
            orf[ind] = [orf[ind].strip()]
    return orf


class PyJQ(object):

    def __init__(self, json_obj, datetime_field=None, datetime_format=_DATETIME_FORMAT):
        """
        Convert the json object to a dict
        """
        if isinstance(json_obj, str):
            self.json = json.loads(json_obj)
        elif isinstance(json_obj, dict):
            self.json = json_obj

        if datetime_field:
            try:
                dt_value = self.get_value(datetime_field)
                self.datetime = datetime.datetime.strptime(dt_value, datetime_format)
            except Exception as e:
                sys.stderr.write(e.__str__() + "\n")

    def get_value(self, key):
        """
        keys should be a single values or nested like 'key1__key2__key3'
        """
        keys = key.split('__')
        dt_value = ''

        try:
            dt_value = reduce(operator.getitem, keys, self.json)
        except TypeError as e:
            err_str = "[-] Type error(%s)" % (e.__str__())
            raise Exception(err_str)  # TODO: logger
        except KeyError as e:
            err_str = "[-] Key error(%s)" % (e.__str__())
            raise Exception(err_str)  # TODO: logger
        return dt_value

    def filter(self, key, op='==', value=None):
        """
        key = 'agent__id'

        # https://docs.python.org/3/library/operator.html
        operator = set( == | in |)

        value = expected

        usage:
            wa.filter('agent__ip') -> return the corresponding value
            wa.filter('agent__ip', '172.16.16.2') -> returns True if match
            wa.filter('rule__description', 'in', 'login') -> returns if 'login' word is in the rule description
        """

        ops = {'==': operator.eq,
               '!=': operator.ne,
               'in': operator.contains,
               '<=': operator.le,
               '<': operator.lt,
               '>': operator.gt,
               '>=': operator.ge,
               # add all the others here
               }
        if op not in ops:
            raise Exception('Invalid operator "{}"'.format(op))
        value_got = str(self.get_value(key)).encode()

        if ops[op](value_got, str(value).encode()):
            return True
        elif value is None:
            return value_got

    def info(self):
        elements = (json.dumps(self.json.get('data', ''), indent=2), str(self.json.get('full_log', '')))
        result = '\n'.join(elements)
        print(result)

    def __str__(self):
        return json.dumps(self.json, indent=2)


class WazuhLogAnalyzer:

    def __init__(self, options, comms):

        # Init options
        self.options = options
        self.callback = options.callback
        self.filters = options.filters
        self.comms = comms

        # Init Thread
        self.log_analyzer_thread = Thread(target=self.handle_stream)
        self.log_analyzer_thread.daemon = True

    def start(self):
        self.log_analyzer_thread.start()

    def jqpy(self, line):
        """
        options could be a custom object with all the fields defined in argparse (see __main__)
        callback_func is the function that will be called on each occurrency
        filters is optional
        """
        jq = PyJQ(line, datetime_field=self.options.datetime_field,  datetime_format=self.options.datetime_format)
        limit_cnt = 0

        if self.options.start_datetime:
            # this is not needed with py37
            start_datetime = datetime.datetime.strptime(self.options.start_datetime,
                                                        _DATETIME_FORMAT_INPUT)
            if jq.datetime < start_datetime.replace(tzinfo=pytz.UTC):
                return
        if self.options.end_datetime:
            # this is not needed with py37
            end_datetime = datetime.datetime.strptime(self.options.end_datetime,
                                                      _DATETIME_FORMAT_INPUT)
            if jq.datetime > end_datetime.replace(tzinfo=pytz.UTC):
                return

        status = True
        and_status = False
        if self.filters:
            filters_blocks = _get_fblocks(self.filters)
            for fi in filters_blocks:
                # and
                if len(fi) > 1:
                    for and_filter in fi:
                        and_status = jq.filter(*and_filter.split(' '))
                        if not and_status:
                            break
                    status = and_status
                if and_status:
                    break

                # or
                if len(fi) == 1:
                    status = jq.filter(*fi[0].split(' '))

        if status:
            # default is print but everything will just work ...
            self.callback(self.comms, jq)
            if self.options.limit:
                limit_cnt += 1
                if limit_cnt == self.options.limit:
                    return

    def handle_stream(self):

        filters = self.options.filters  # got something like ['agent__ip', '==', '172.16.16.254']

        if self.options.callback:
            package, function = self.options.callback.rsplit('.', 1)
            package = import_module(package)
            self.callback = getattr(package, function)
        else:
            self.callback = print

        if self.options.json:
            file_name = self.options.json
            try:
                file = open(file_name, 'r')
                print(file_name)
                content = file.read()
            except IOError as e:
                print("[-] I/O error(%s): %s" % (e.errno, e.strerror))  # TODO: logger
                sys.exit(1)
            except UnicodeDecodeError:  # Try to handle it a gzip
                file = gzip.open(file_name, 'rb')
                content = file.read().decode()
        else:  # standard input handler
            file = content = sys.stdin
            content = '{}'

        try:  # handle a pure json file or multiple json objects delimited by newline (\n)
            content_dict = json.loads(content)
            self.jqpy(content_dict)

        except json.decoder.JSONDecodeError as e:  # TODO: suppose it is file consisting os newline separated JSON instances
            sys.stderr.write('[-] This is not a pure json file "{}"\n'.format(e))
            if self.options.json:
                # start from the beginning
                file.seek(0)
                lines = iter(file.readlines())
            else:  # stdin
                lines = content.splitlines()
            for line in lines:
                if self.options.echo:
                    print(line)
                    continue
                else:
                    line = line.decode() if isinstance(line, bytes) else line
                    self.jqpy(line)
        except UnboundLocalError as e:
            sys.stderr.write("[-] UnboundLocalError {}".format(e))


        # realtime or standard input driven streams takes only the latest entries
        if self.options.realtime or isinstance(file, io.TextIOBase):
            if self.options.json:
                # standard input blocks the reading ...
                # if realtime ignores the older data
                file.read()

            _err_msg = '[-] Missed chunks from realtime reading: {}. Error: {}.\n'
            _stderr_msg = ''
            while 1:
                line_buffer = ''
                time.sleep(self.options.timeout)
                digits = file.read(self.options.bufsize)
                dec_digits = digits.decode() if isinstance(digits, bytes) else digits
                if not dec_digits:
                    continue
                line_buffer += dec_digits
                line_buffer = line_buffer.strip('\n')
                chunks = line_buffer.split(_REALTIME_DELIM)
                for chunk in chunks:
                    try:
                        self.jqpy(chunk)
                    except json.decoder.JSONDecodeError as e:
                        _err = _err_msg.format(chunk[: 33] + '[ ... ]', e)
                        if _stderr_msg != _err:
                            _stderr_msg = _err
                            sys.stderr.write(_stderr_msg)

        else:
            if self.options.json:
                # start from the beginning
                file.seek(0)
                lines = iter(file.readlines())
            else:
                # stdin
                lines = content.splitlines()
            for line in lines:
                if self.options.echo:
                    print(line)
                    continue
                else:
                    line = line.decode() if isinstance(line, bytes) else line
                    self.jqpy(line)


