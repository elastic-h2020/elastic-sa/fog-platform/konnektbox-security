import json


# Callback function for Wazuh Log analyzer
def mqtt_pub_callback(mqtt_client, obj):
    # Add level(severity) field in JSON
    try:
        json_alert = obj.json["agent"]
        json_alert["level"] = obj.json["rule"]["level"]
        print(json_alert)
        print("Publishing message to topic", "/elastic/security/agent")
        mqtt_client.send(json.dumps(json_alert), "/elastic/security/agent")
    except:
        pass